<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="contact-info" class="spacer">
		<div class="wrapper">

			<div class="field phone">
				<h5>Phone</h5>
				<p><?php the_field('phone', 'options'); ?></p>
			</div>

			<div class="field email">
				<h5>Email</h5>
				<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
			</div>

			<div class="field address">
				<h5>Address</h5>
				<p><?php the_field('address', 'options'); ?></p>
			</div>

		</div>
	</section>
	

	<section id="map">
		<a href="<?php the_field('map_link'); ?>" rel="external">
			<span class="cover map-image" style="background-image: url(<?php $image = get_field('map'); echo $image['url']; ?>);"></span>
		</a>
	</section>

<?php get_footer(); ?>