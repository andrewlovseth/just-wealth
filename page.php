<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section id="main" class="generic">
			<div class="wrapper">

				<h2><?php the_title(); ?></h2>			
				<?php the_content(); ?>

			</div>
		</section>

	<?php endwhile; endif; ?>;

<?php get_footer(); ?>