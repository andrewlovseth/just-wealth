<?php get_header(); ?>

	<section id="blog-header">
		<div class="wrapper">

			<h1>The Guidebook</h1>

		</div>
	</section>

	<section id="posts">
		<div class="wrapper">

			<div class="posts-wrapper">
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

					<?php get_template_part('partials/post-teaser'); ?>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>
	
<?php get_footer(); ?>