$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav').toggleClass('open');
		return false;
	});

	$('nav a.inactive').on('click', function(){
		return false;
	});



	
});