	<footer>
		<div class="wrapper">

			<div class="footer-nav">
				<a href="<?php echo site_url('/'); ?>" class="logo">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<?php if(have_rows('footer_nav', 'options')): while(have_rows('footer_nav', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>">
				        <?php the_sub_field('label'); ?>
				    </a>

				<?php endwhile; endif; ?>

			</div>

			<div class="disclaimer">
				<?php the_field('footer_disclaimer', 'options'); ?>
			</div>


		</div>
	</footer>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>