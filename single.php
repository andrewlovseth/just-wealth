<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article>
			<div class="wrapper">

				<section id="article-header">
					<h5><?php the_time('F j, Y'); ?></h5>
					<h2><?php the_title(); ?></h2>
				</section>

				<section id="featured-photo">
					<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</section>

				<section id="article-body">
					<?php the_content(); ?>
				</section>

			</div>
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>