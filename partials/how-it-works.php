<section id="how-it-works">
	<div class="wrapper">

		<div class="info">
			<h3><?php the_field('how_it_works_headline'); ?></h3>

			<div class="items">
				<?php if(have_rows('how_it_works')): while(have_rows('how_it_works')): the_row(); ?>
				 
				    <div class="item">
				        <?php the_sub_field('copy'); ?>
				    </div>

				<?php endwhile; endif; ?>
			</div>
		</div>

	</div>
</section>