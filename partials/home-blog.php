<section id="blog-header">
	<div class="wrapper">

		<h1>The Guidebook</h1>

	</div>
</section>

<section id="posts">
	<div class="wrapper">

		<div class="posts-wrapper">

			<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				<?php get_template_part('partials/post-teaser'); ?>

			<?php endwhile; endif; wp_reset_postdata(); ?>
			
		</div>

	</div>
</section>