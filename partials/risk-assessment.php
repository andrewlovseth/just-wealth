<section id="risk-assessment">
	<div class="wrapper">

		<div class="info">
			<h3>Take your Riskalyze test now</h3>
			<p>Answer this survey to assess your own tolerance to risk in your portfolio.</p>
		</div>

		<div class="cta">
			<a href="#" class="btn">Take the test</a>
		</div>

	</div>
</section>