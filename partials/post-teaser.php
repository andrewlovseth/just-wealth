<article>
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<h5><?php the_time('F j, Y'); ?></h5>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php the_excerpt(); ?>

		<a href="<?php the_permalink(); ?>" class="underline">Read more</a>
	</div>
</article>