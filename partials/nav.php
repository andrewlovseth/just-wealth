<nav>
	<div class="wrapper">

		<div id="nav-links">

			<div class="nav-link portfolio-checkup">
				<a href="<?php echo site_url('/how-we-differ/'); ?>">How We Differ</a>
			</div>

			<div class="nav-link investment-consulting">
				<a href="<?php echo site_url('/investment-philosophy/'); ?>">Investment Philosophy</a>
			</div>

			<div class="nav-link get-started">
				<a href="<?php echo site_url('/get-started/'); ?>">Get Started</a>
			</div>

			<div class="nav-link about">
				<a href="<?php echo site_url('/about/'); ?>">About</a>
			</div>

			<div class="nav-link contact">
				<a href="<?php echo site_url('/contact/'); ?>">Contact</a>
			</div>

			<div class="nav-link guidebook">
				<a class="inactive" href="#">The Guidebook <span>Coming Soon</span></a>
			</div>

		</div>

		<div id="nav-contact-info">
			<div class="field phone">
				<p><?php the_field('phone', 'options'); ?></p>
			</div>

			<div class="field email">
				<p>
					<a href="mailto:<?php the_field('email', 'options'); ?>">
						<?php the_field('email', 'options'); ?>
					</a>
				</p>
			</div>

			<div class="field address">
				<p><?php the_field('address', 'options'); ?></p>
			</div>
		</div>

	</div>
</nav>	