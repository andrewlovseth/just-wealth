<section id="hero">
	<div class="wrapper">
		<div class="headline">
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php if(get_field('hero_subheadline')): ?>
				<h2><?php the_field('hero_subheadline'); ?></h2>
			<?php endif; ?>
		</div>
	</div>

	<div class="image cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">

		<section id="call-out">

			<div class="header">
				<h2>
					<span class="value"><?php the_field('call_out_value'); ?></span>
					<span class="key"><?php the_field('call_out_key'); ?></span>
				</h2>
			</div>

			<div class="deck">
				<?php the_field('call_out_deck'); ?>
			</div>

		</section>

	</div>
</section>