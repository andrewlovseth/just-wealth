<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="about-just-wealth" class="content spacer">
		<div class="wrapper">

			<div class="info">
				<h3><?php the_field('about_just_wealth_headline'); ?></h3>
				<?php the_field('about_just_wealth_copy'); ?>
			</div>

		</div>
	</section>

	<section id="about-heide" class="content">

		<div class="photo cover" style="background-image: url(<?php $image = get_field('about_heide_photo'); echo $image['url']; ?>);">
		</div>

		<div class="info">
			<div class="info-wrapper">
				<h3><?php the_field('about_heide_headline'); ?></h3>
				<?php the_field('about_heide_copy'); ?>
			</div>
		</div>

	</section>

<?php get_footer(); ?>