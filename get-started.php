<?php

/*

	Template Name: Get Started

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="get-started-steps" class="content spacer">
		<div class="wrapper">


			<div class="info">

				<h3><?php the_field('get_started_headline'); ?></h3>

				<div class="items">
					<?php if(have_rows('get_started')): while(have_rows('get_started')): the_row(); ?>
					 
					    <div class="item">
					    	<div class="copy">
						        <?php the_sub_field('copy'); ?>					    		
					    	</div>

					    	<?php if(get_sub_field('graphic')): ?>
					    		<div class="image">
					    			<img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</div>
					    	<?php endif; ?>
					    </div>

					<?php endwhile; endif; ?>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>