<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero">
		<div class="wrapper">
			<div class="headline">
				<h1><?php the_field('hero_headline'); ?></h1>
			</div>
		</div>

		<div class="image cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">

			<section id="call-out">
				<div class="header">
					<h2>
						<span class="value"><?php the_field('call_out_value'); ?></span>
						<span class="key"><?php the_field('call_out_key'); ?></span>
					</h2>
				</div>

				<div class="deck">
					<?php the_field('call_out_deck'); ?>
				</div>
			</section>

		</div>
	</section>

	<section id="about-statement" class="cover">
		<div class="wrapper">

			<?php the_field('about_statement'); ?>


		</div>
	</section>

	<section id="what-we-do">
		<div class="wrapper">

			<?php the_field('what_we_do'); ?>

		</div>
	</section>


	<section id="about">
		<div class="photo cover" style="background-image: url(<?php $image = get_field('about_photo'); echo $image['url']; ?>);">
		</div>

		<div class="info">

			<div class="info-wrapper">
				<h3><?php the_field('about_headline'); ?></h3>
				<?php the_field('about_deck'); ?>

				<a href="<?php echo site_url('/about/'); ?>" class="underline">Learn more</a>
			</div>
		</div>
	</section>


	<?php // get_template_part('partials/home-blog'); ?>

<?php get_footer(); ?>