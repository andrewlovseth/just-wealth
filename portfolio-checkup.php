<?php

/*

	Template Name: Portfolio Checkup

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="them-vs-us" class="spacer content">
		<div class="wrapper">

			<div class="info">
				<?php the_field('them_vs_us_copy'); ?>
			</div>

			<div class="differences">
				<div class="feature feature-header">
					<div class="us">
						<h3>Just Wealth</h3>
					</div>

					<div class="them">
						<h3>Typical Advisor</h3>
					</div>
				</div>
				<?php if(have_rows('them_vs_us_table')): while(have_rows('them_vs_us_table')): the_row(); ?>
					<div class="feature">
						<div class="us">
							<h4>Just Wealth</h4>
							<p><?php the_sub_field('us'); ?></p>
						</div>	

						<div class="them">
							<h4>Typical Advisor</h4>
							<p><?php the_sub_field('them'); ?></p>
						</div>				
					</div>
				<?php endwhile; endif; ?>
			</div>
			

		</div>
	</section>

	<section id="fees" class="content">
		<div class="wrapper">

			<div class="headline">
				<h3><?php the_field('fees_headline'); ?></h3>
			</div>

			<div class="info">
				<?php the_field('fees_copy'); ?>
			</div>

			<div class="illustration">
				<div class="illustration-wrapper">
					<?php the_field('fees_illustration'); ?>
				</div>
			</div>

		</div>
	</section>



<?php get_footer(); ?>