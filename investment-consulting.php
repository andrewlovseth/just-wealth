<?php

/*

	Template Name: Investment Consulting

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="philosophy" class="spacer content">
		<div class="wrapper">
			
			<div class="info">
				<h3><?php the_field('philosophy_headline'); ?></h3>
				<?php the_field('philosophy_copy'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>